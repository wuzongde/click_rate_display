# 点击率展示图
地图展示点击率

## 目录部分说明
├── admin.py<br>
├── apps.py<br>
├── __init__.py<br>
├── models.py<br>
├── static 静态文件<br>
├── templates<br>
│   ├── map 地图json文件<br>
│   ├── map.html 页面<br>
├── tools 工具类<br>
│   ├── addmongo.py mongodb操作<br>
│   ├── addmysql.py mysql操作<br>
│   ├── ipquery.py ip转换成省市地址<br>

## 效果展示

![输入图片说明](https://gitee.com/uploads/images/2018/0222/152737_4443b371_904730.png "map1.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0222/160325_e54450ad_904730.gif "map7.gif")