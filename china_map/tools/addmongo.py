#!/usr/local/python36/bin/python3.6
#-*- coding:utf-8 -*-

from datetime import datetime, timedelta
from bson.errors import InvalidDocument
import re

rr = re.compile('\.')
def insert_mongo_all(newdb,request_data_list):
    #date1 = datetime.now()
    #date2 = datetime.strftime(date1, "%Y%m%d")
    GMT_FORMAT = '%b %d, %Y %I:%M:%S %p'
    for i in request_data_list:
        try:
            i["requestAt"] = datetime.strptime(i["requestAt"], GMT_FORMAT)
            try:
                parameters_json = i["parameters"]
                for k, v in parameters_json.items():
                    if rr.findall(k) != []:
                        new_key = ''.join(k.split('.'))
                        parameters_json.pop(k)
                        parameters_json[new_key] = v
                        print(parameters_json)
            except BaseException as b:
                _log_err = "insert_mongo_all函数异常 --》" + str(b)
                print(_log_err)
            finally:
                newdb.requestStatistics.insert_one(i)
        except BaseException as b:
            _log_err = "insert_mongo_all函数异常 --》" + str(b)
            print(_log_err)

