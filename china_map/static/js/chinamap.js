/**
 * Created by jiexin on 2017/10/20.
 */

// 中国地图
var allmapa = document.getElementById("map");
var allmapw = allmapa.offsetWidth - 130;
var geoCoordMap = {
    '北京': [116.4551, 40.2539],
    '天津': [117.4219, 39.4189],
    '河北': [114.4995, 38.1006],
    '山西': [112.3352, 37.9413],
    '内蒙古': [111.4124, 40.4901],
    '辽宁': [123.1238, 42.1216],
    '吉林': [125.8154, 44.2584],
    '黑龙江': [127.9688, 45.368],
    '上海': [121.4648, 31.2891],
    '江苏': [118.8062, 31.9208],
    '安徽': [117.29, 32.0581],
    '福建': [119.4543, 25.9222],
    '江西': [116.0046, 28.6633],
    '山东': [117.1582, 36.8701],
    '河南': [113.4668, 34.6234],
    '湖北': [114.3896, 30.6628],
    '湖南': [113.0823, 28.2568],
    '广东': [113.5107, 23.2196],
    '广西': [108.479, 23.1152],
    '海南': [110.3893, 19.8516],
    '重庆': [107.7539, 30.1904],
    '四川': [103.9526, 30.7617],
    '贵州': [106.6992, 26.7682],
    '云南': [102.9199, 25.4663],
    '西藏': [91.1865, 30.1465],
    '陕西': [109.1162, 34.2004],
    '甘肃': [103.5901, 36.3043],
    '青海': [101.4038, 36.8207],
    '宁夏': [106.3586, 38.1775],
    '新疆': [87.9236, 43.5883],
    '台湾': [121.509062,25.044332],
    '澳门': [113.54909,22.198951],
    '香港': [114.173355,22.320048],
    '浙江': [119.5313, 29.8773],
    '杭州': [120.153576,30.287459] 
};
var color = ['#a6c84c', '#ffa022', '#46bee9'];
var allmapseries = [];
var BJData = [];
var thadata = [];

function eachdata(orgmap) {
    BJData = [];
    thadata = [];
    $.each(orgmap, function (k,v) {
        if (v > 0){
            BJData.push([{name:k, value:v}, {name:'浙江'}]);
            thadata.push(k)
        }
    })
    //console.log(BJData,'111111')
}


var convertData = function (data) {
    var res = [];
    for (var i = 0; i < data.length; i++) {
        var dataItem = data[i];
        var fromCoord = geoCoordMap[dataItem[0].name];
        var toCoord = geoCoordMap[dataItem[1].name];
       	if (fromCoord && toCoord) {
            res.push([
            	{
                	coord:fromCoord,
                	value: dataItem[0].value
	            },
	            {
	            	coord: toCoord
	            }
            ]);
        }
    }
    return res;
};

var allmapoption = {
    textStyle: {
        fontSize:16,
        color: '#4af9d6'
    },
    tooltip : {
		trigger : 'item',
		backgroundColor:'rgba(12, 204, 104, 0.92)',
		borderColor:'#FFFFCC',
		showDelay : 0,
		hideDelay : 0,
		enterable:true,
		transitionDuration : 0,
		extraCssText:'z-index:100',
		formatter : function(params, ticket, callback) {
		    //根据业务自己拓展要显示的内容
            if (params.value[2] !== undefined && params.data['name'] !== '杭州'){
                var res = "";
                var name = params.name;
                var value = params.value[2];
                res = "<span style='color:red; font-size: 30px;'>"+name+"</span><br/><span style='font-size: 20px;'>点击量："+value+"</span>";
                return res;
            }
		}
	},
	visualMap: { //图例值控制
        min : 0,
        max : 10000,
        show: false,
        calculable : true,
        color: ['#ff3333', 'orange', 'yellow','lime','aqua'],
        textStyle:{
            color:'#fff'
        }
    },
	geo: {
		map: 'china',
		label: {
			emphasis: {
				show: false
			}
		},

		roam: true, //是否允许缩放
		layoutCenter: ['50%', '50%'], //地图位置
		// layoutSize:"108%",
		layoutSize:allmapw,
		itemStyle: {
			normal: {
			    borderWidth:2,
				color: 'rgba(51, 69, 89, 0)', //地图背景色
				borderColor: 'rgba(100,149,237,10)' //省市边界线
			},
			emphasis: {
				color: 'rgba(37, 43, 61, 0)'//悬浮背景
			}
		}

	},
    series: allmapseries
};

function createseries() {
    [['杭州', BJData]].forEach(function (item, i) {
        allmapseries.push(
          {
            type: 'lines',
            name: 'a',
            zlevel: 2,
            effect: {
                show: false,
                period: 4, //箭头指向速度，值越小速度越快
                trailLength: 0.02,//特效尾迹长度[0,1]值越大，尾迹越长重
                symbol:'arrow',//箭头图标
                symbolSize: 10//图标大小
            },
            lineStyle: {
                normal: {
                    width: 2,//尾迹线条宽度
                    opacity: 0,//尾迹线条透明度
                    curveness: 0 //尾迹线条曲直度
                }
            },

            data: convertData(item[1])
        },
        {
            type: 'effectScatter',
            coordinateSystem: 'geo',
            name: 'b',
            zlevel: 2,
            rippleEffect: {//涟漪特效
                period:4,//动画时间，值越小速度越快
                brushType: 'stroke', //波纹绘制方式 stroke, fill
                scale: 4 //波纹圆环最大限制，值越大波纹越大
            },
            label: {
                normal: {
                    show: true,
                    position: 'top',//显示位置
                    offset:[0, -12], //偏移设置
                    fontSize: 18,
                    formatter: '{b}' //圆环显示文字
                },
                emphasis: {
                    show: true
                }
            },
            symbol: 'circle',
            symbolSize: function (val) {
                return 18; //圆环大小
            },
            itemStyle: {
                normal: {
                    show: false,
                    color: '#f00'
                }
            },
            data: item[1].map(function (dataItem) {
                return {
                    name: dataItem[0].name,
                    value: geoCoordMap[dataItem[0].name].concat([dataItem[0].value])
                };
            })
        },
        //中心
        {
            type: 'scatter',
            name: 'c',
            coordinateSystem: 'geo',
            zlevel: 2,
            rippleEffect: {
                period:4,
                brushType: 'stroke',
                scale: 4
            },
            label: {
                normal: {
                    show: true,
                    position: 'right',
    //			                offset:[5, 0],

                    color:'#00ffff',
                    formatter: '{b}',
                    textStyle: {
                        fontSize: 30,
                        color:"#fbe90f"
                    }
                },
                emphasis: {
                    show: true
                }
            },
            symbol: 'pin',
            symbolSize:60,
            itemStyle: {
                normal: {
                    show: true,
                    color: '#9966cc'
                }
            },
            data:[{
                name: item[0],
                value: geoCoordMap[item[0]].concat([100000])
            }]
        });
    });
}

var newallmapseries;
function createseriesT() {
    newallmapseries = []
    var fordata = [['杭州', BJData]];
    console.log(fordata);
    fordata.forEach(function (item, i) {
        newallmapseries.push(
        {
            name: 'a',
            data: convertData(item[1])
        },
        {
            name: 'b',
            data: item[1].map(function (dataItem) {
                return {
                    name: dataItem[0].name,
                    value: geoCoordMap[dataItem[0].name].concat([dataItem[0].value])
                };
            })
        },
        //中心
        {
            name: 'c',
            data:[{
                name: item[0],
                value: geoCoordMap[item[0]].concat([100000])
            }]
        }
      )
    })
}

setTimeout(function() {
     autoHoverTip();
     tv = setInterval(autoHoverTip, counts*5600);
}, 500);


eachdata(maps);
createseries();
var allmapChart = echarts.init(document.getElementById('map'));
allmapChart.setOption(allmapoption);
//点击事件
allmapChart.on('click', function(params){
    if (thadata.indexOf(params.name) !== -1) {
        city = params.name;
        map_name = provincesTextJson[city];
        //console.log(city,map_name)
        showProvince();
    }
});

//var counts;
var counts = allmapChart.getOption().series[0].data.length; //获取所有闪动圆环数量
var dataIndex = 0;
//让圆环的提示框自动触发轮播显示
function autoHoverTip(){
   for(var i = 0;i<counts;i++){
       (function (i) {
             ts = setTimeout(function () {
                allmapChart.dispatchAction({
                    type: 'showTip',
                    seriesIndex: 1,
                    dataIndex: i
                });
                 }, 5000*i);
            })(i);
    }
}

//省
var provinces = ['shanghai', 'hebei','shanxi','neimenggu','liaoning','jilin','heilongjiang','jiangsu','zhejiang','anhui','fujian','jiangxi','shandong','henan','hubei','hunan','guangdong','guangxi','hainan','sichuan','guizhou','yunnan','xizang','shanxi','gansu','qinghai','ningxia','xinjiang', 'beijing', 'tianjin', 'chongqing', 'xianggang', 'aomen', 'taiwan'];
var provincesText = ['上海', '河北', '山西', '内蒙古', '辽宁', '吉林','黑龙江',  '江苏', '浙江', '安徽', '福建', '江西', '山东','河南', '湖北', '湖南', '广东', '广西', '海南', '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆', '北京', '天津', '重庆', '香港', '澳门','台湾'];
var map_name = 'shanxi';
var getjson;
var city = '山西';
var provincesJson = {};
var provincesTextJson = {};
for (var i=0;i<provincesText.length;i+=1) {
    provincesJson[provinces[i]] = provincesText[i];
    provincesTextJson[provincesText[i]] = provinces[i];
}

var showProvincea = document.getElementById("right_top");
var showProvinceh = allmapa.offsetHeight - allmapa.offsetHeight * 0.75;
var othercity = [];
var othermax;

//console.log(showProvinceh);
function showProvince() {
    $.ajax({
        type: "POST",
        url: "/map/",
        data: {'mapname':map_name + '.json','mapcity':city, 'status':1},
        dataType: "json",
        success: function (datas) {
            var otherlistv = [];
            var otherlistk = [];
            $.each(datas["othercity"], function (k,v) {
                othercity.push({name:k, value:v});
                otherlistk.push(k);
                otherlistv.push(v)
            });
            getjson = datas['othermap'];
            othermax = datas['othermax'];
            //console.log(othercity);
            othChart = echarts.init(document.getElementById('other_map'));
            echarts.registerMap(map_name, getjson);
            othChart.setOption(option = {
                title: {
                    text: city,
                    left: 'center',
                    textStyle: {
                        color: '#fff'
                    }
                },
                visualMap: {
                    min: 0,
                    max: othermax,
                    show: false,
                    left: 'left',
                    top: 'bottom',
                    calculable: true,
                    color: ['#5505f9', '#05f9eb'],
                    textStyle:{
                        color:"#fff"
                    }
                },
                brush: {
                    outOfBrush: {
                        color: '#abc'
                    },
                    brushStyle: {
                        borderWidth: 2,
                        color: 'rgba(0,0,0,0.2)',
                        borderColor: 'rgba(0,0,0,0.5)'
                    },
                    seriesIndex: [0, 1],
                    throttleType: 'debounce',
                    throttleDelay: 300,
                    geoIndex: 0
                },
                tooltip: {
                    trigger: 'item'
                },
                grid: {
                    right: 50,
                    width: '30%'
                },
                xAxis: {
                    zlevel:2,
                    type: 'value',
                    scale: true,
                    position: 'top',
                    boundaryGap: false,
                    splitLine: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        margin: 2,
                        textStyle: {
                            color: '#aaa'
                        }
                    }
                },
                yAxis: {
                    zlevel:2,
                    type: 'category',
                    name: '点击量',
                    nameGap: 16,
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ddd'
                        }
                    },
                    axisTick: {
                        show: false,
                        lineStyle: {
                            fontSize: 10,
                            color: '#ddd'
                        }
                    },
                    axisLabel: {
                        interval: 0,
                        textStyle: {
                            fontSize: 10,
                            color: '#ddd'
                        }
                    },
                    data:  otherlistk
                },
                series: [
                    {
                        roam: true,
                        name: '用户点击量',
                        layoutCenter: ['20%', '45%'],
                        layoutSize: showProvinceh,
                        type: 'map',
                        mapType: map_name,
                        itemStyle: {
                            normal:{
                                areaColor: '#05f9eb'
                            },
                            emphasis: {
                                areaColor:'#0af787'
                            }
                        },
                        label: {
                            normal: {
                                show: false
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize:18,
                                    color: "#fff"
                                }
                            }
                        },
                        data:othercity,
                        animation: false
                    },
                    {
                        id: 'bar',
                        type: 'bar',
                        symbol: 'none',
                        layoutCenter: ['70%', '45%'],
                        layoutSize: showProvinceh,
                        itemStyle: {
                            normal: {
                                color: '#ddb926'
                            }
                        },
                        data: otherlistv
                    }
                ]
            })
        }
    })
}
showProvince();

var shopbarchart = echarts.init(document.getElementById('top_map'));
function shopbarcreate(){
    var shopbaroption = {
        tooltip: {
        },
        angleAxis: {
            type: 'category',
            data: shop_top_name,
            z: 10,
            axisLine: {
                show: false,
                lineStyle:{
                    color: '#85f5fb',
                    width: 5
                }
            },
            axisLabel: {

                textStyle: {
                    fontSize:20,
                    color:"#fff"
                },
                formatter: function (parems) {
                    //console.log(parems);
                    var ress = "";
                    var namedata = shop_top_data[shop_top_name.indexOf(parems)];
                    ress = namedata.toString();
                    return ress
                }
            },
            splitLine: {
                show:false
            }
        },
        radiusAxis: {
            show:false,
            name: '导入店铺数量'
        },
        itemStyle: {
            normal: {
                color: '#68a2a9'
            }
        },
        polar: {},
        series: [{
            type: 'bar',
            data: shop_top_data,
            coordinateSystem: 'polar',
            label:{
                normal: {
                    show:true
                },
                emphasis :{
                    show:true
                }
            },
            itemStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: '#00FFE6'
                        }, {
                            offset: 1,
                            color: '#007CC6'
                        }]),
                    borderWidth: 0,
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(71, 251, 185, 0.8)'
                },
                emphasis: {
                    borderWidth: 8,
                    shadowBlur: 20,
                    shadowOffsetX: 1,
                    shadowColor: 'rgba(255, 255, 255, 1)'
                }
            }
        }]
    };
    shopbarchart.setOption(shopbaroption)
}
shopbarcreate();





Date.prototype.format = function(fmt) { //author: meizz
    var o = {
            "M+" : this.getMonth()+1,                 //月份
            "d+" : this.getDate(),                    //日
            "h+" : this.getHours(),                   //小时
            "m+" : this.getMinutes(),                 //分
            "s+" : this.getSeconds(),                 //秒
            "q+" : Math.floor((this.getMonth()+3)/3),   //季度
            "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
};

function updatetime() {
    $.ajax({
        type: "POST",
        url: "/map/",
        data: {'status':0},
        dataType: "json",
        success: function (datas) {
            eachdata(datas["map"]);
            //allmapseries = [];
            createseriesT();
            //var returnoption = allmapChart.getOption()
            //returnoption.series = allmapseries
            //allmapChart.setOption(returnoption);
            console.log(111111111111111111111111);
            allmapChart.setOption({'series':newallmapseries});
        }
    })
}


var updatedata;
<!--页面加载完成后事件-->
$(document).ready(function () {
    updatedata = setInterval(function(){
        updatetime();
        console.log('启动定时任务')
    },120000);
});

$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "/cycle1/",
        data: {'status':0}
    });
    $.ajax({
        type: "POST",
        url: "/cycle2/",
        data: {'status':0}
    })
});







