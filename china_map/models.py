from django.db import models
import django.utils.timezone as timezone
# Create your models here.

class u_city_s(models.Model):
    ucId = models.BigAutoField(max_length=30, primary_key=True, null=False)
    cityId = models.BigIntegerField(blank=False)
    cityname = models.FileField(max_length=255,null=False)
    provinceId = models.BigIntegerField(blank=False)
    count = models.IntegerField(null=False)
    createTime = models.DateTimeField(default = timezone.now)

    class Meta:
        db_table = "u_city_s"
        # abstract = True


class u_province_s(models.Model):
    upId = models.BigAutoField(max_length=30, primary_key=True, null=False)
    provinceId = models.BigIntegerField(blank=False)
    provincename = models.FileField(max_length=255,null=False)
    count = models.IntegerField(null=False)
    createTime = models.DateTimeField(default = timezone.now)

    class Meta:
        db_table = "u_province_s"
        # abstract = True


class c_city_s(models.Model):
    cityId = models.BigIntegerField(primary_key=True, null=False)
    cityname = models.FileField(max_length=255,null=False)
    provinceId = models.BigIntegerField(blank=False)

    class Meta:
        db_table = "c_city_s"
        # abstract = True

class c_province_s(models.Model):
    provinceId = models.BigIntegerField(primary_key=True, null=False)
    provincename = models.FileField(max_length=255,null=False)

    class Meta:
        db_table = "c_province_s"
        # abstract = True

class all_request(models.Model):
    requestId = models.BigAutoField(max_length=30, primary_key=True, null=False)
    origin = models.FileField(max_length=255, null=True)
    requestAt = models.DateTimeField(null=True)
    url = models.FileField(max_length=255, null=True)
    parameters = models.FileField(max_length=10000, null=True)
    consume = models.FileField(max_length=255, null=True)
    sessionId = models.FileField(max_length=255, null=True)
    createTime = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = "all_request"
        # abstract = True
